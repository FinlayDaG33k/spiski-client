# Spiski
List app for near-realtime synching with others.

# Why Spiski?
Spiski (Russian for "Lists") is basicaly yet another listkeeping app, ideal for having grocery lists, checklists for an event or checklists for something else.  

A long time ago, I made the app called "[MijnLijstjes](https://gitlab.com/FinlayDaG33k/MijnLijstjes)" but this wasn't a great app to maintain (because I made tons of spaghetti instead of an app) and didn't have much demand.  
Recently, however, my girlfriend was looking for an app similar to it but I alredy abandoned MijnLijstjes.  
Since I also needed a similar app again, I decided to revamp MijnLijstjes but instead of trying to overhaul that codebase, I just started from scratch instead.